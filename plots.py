import sys
import json
import matplotlib.pyplot as plt
import numpy as np

def plot(data, stat, ops, title, filename):
    fig,ax = plt.subplots()

    bars = len(data[ops[0]].keys())*2
    width = .8/bars
    i = 0
    for op in ops:
        for k in data[op].keys():
            for op1 in ["read","write"]:
                if data[op][k][stat][op1][0] == 0: continue
                x = np.arange(len(data[op][k]['depth']))
                x = x-(width*bars/2)+width*i+width/2
                i += 1
                ax.bar(x, data[op][k][stat][op1], width, label=f"{k} {op1}")

    if stat == "bw":
        ax.set_ylabel("Bandwidth (MiB/s)")
    elif stat == "lat":
        ax.set_ylabel("Latency (us)")
    ax.set_xlabel("iodepth")
    ax.set_xticks(np.arange(len(data[op][k]['depth'])))
    ax.set_xticklabels(data[op][k]['depth'])
    ax.legend()
    ax.set_title(title)
    # plt.show()
    plt.savefig(filename)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print(f"usage: {sys.argv[0]} [input json file] [output prefix]")
        sys.exit(1)
    
    infilename = sys.argv[1]
    prefix = sys.argv[2]

    infile = open(infilename)
    if infile is None:
        print("could not open file")
        sys.exit(1)

    jfile = json.load(infile)

    jobs = len(jfile["jobs"])
    print(f"found {jobs} jobs")

    data = {}

    for i in range(jobs):
        name = jfile["jobs"][i]["jobname"]
        test = name.split("_")[1]
        print(test)
        op = jfile["jobs"][i]["job options"]["rw"]
        print(f"job {i} name: {name}, type: {op}")
        bs = jfile["jobs"][i]["job options"]["bs"]
        depth = jfile["jobs"][i]["job options"]["iodepth"]
        bw_read = jfile["jobs"][i]["read"]["bw"]/1024
        lat_read = jfile["jobs"][i]["read"]["lat_ns"]["mean"]/1e6
        bw_write = jfile["jobs"][i]["write"]["bw"]/1024
        lat_write = jfile["jobs"][i]["write"]["lat_ns"]["mean"]/1e6

        if test not in data: data[test] = {}
        if bs not in data[test]: data[test][bs] = {'depth':[], 'bw':{"read":[], "write":[]}, 'lat':{"read":[], "write":[]}}
        data[test][bs]['depth'].append(depth)
        data[test][bs]['bw']['read'].append(bw_read)
        data[test][bs]['lat']['read'].append(lat_read)
        data[test][bs]['bw']['write'].append(bw_write)
        data[test][bs]['lat']['write'].append(lat_write)

    print(data)

    plot(data, "bw", ["r","w"], "Sequential read/write only bandwidth", f"{prefix}_sqonlyband.png")
    plot(data, "lat", ["r","w"], "Sequential read/write only latency", f"{prefix}_sqonlylat.png")

    plot(data, "bw", ["rw"], "Sequential simultaneous read/write bandwidth", f"{prefix}_sqsimband.png")
    plot(data, "lat", ["rw"], "Sequential simultaneous read/write latency", f"{prefix}_sqsimlat.png")

    plot(data, "bw", ["r30w"], "Sequential simultaneous 30% read / 70% write bandwidth", f"{prefix}_sqsim30band.png")
    plot(data, "lat", ["r30w"], "Sequential simultaneous 30% read / 70% write latency", f"{prefix}_sqsim30lat.png")
    
    plot(data, "bw", ["r70w"], "Sequential simultaneous 70% read / 30% write bandwidth", f"{prefix}_sqsim70band.png")
    plot(data, "lat", ["r70w"], "Sequential simultaneous 70% read / 30% write latency", f"{prefix}_sqsim70lat.png")