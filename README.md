# Advanced Computer Systems Mini-Project 3 - Memory and Storage Performance Profiling

**Please see 6961_proj3.pdf for this projects report.**


Disk test script usage
---

use the following to generate result json file
`fio --output-format=json disk_test.fio > out.json`

use the following to generate plots from result json file
`python3 plots.py [input json file] [output prefix]`

