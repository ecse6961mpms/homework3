Intel(R) Memory Latency Checker - v3.9
Measuring idle latencies (in ns)...
		Numa node
Numa node	     0	
       0	  64.7	

Measuring Peak Injection Memory Bandwidths for the system
Bandwidths are in MB/sec (1 MB/sec = 1,000,000 Bytes/sec)
Using all the threads from each core if Hyper-threading is enabled
Using traffic with the following read-write ratios
ALL Reads        :	26507.7	
3:1 Reads-Writes :	26127.3	
2:1 Reads-Writes :	24742.5	
1:1 Reads-Writes :	24956.7	
Stream-triad like:	24510.8	

Measuring Memory Bandwidths between nodes within system 
Bandwidths are in MB/sec (1 MB/sec = 1,000,000 Bytes/sec)
Using all the threads from each core if Hyper-threading is enabled
Using Read-only traffic type
		Numa node
Numa node	     0	
       0	25627.3	

Measuring Loaded Latencies for the system
Using all the threads from each core if Hyper-threading is enabled
Using Read-only traffic type
Inject	Latency	Bandwidth
Delay	(ns)	MB/sec
==========================
 00000	141.38	  26089.2
 00002	169.41	  25916.3
 00008	148.25	  26101.6
 00015	153.80	  25212.6
 00050	130.82	  23110.8
 00100	 91.58	  17334.2
 00200	 75.85	  11077.2
 00300	 74.81	   8038.9
 00400	 77.26	   6320.5
 00500	 68.26	   5348.2
 00700	 67.39	   4051.1
 01000	 67.20	   3238.1
 01300	 67.83	   2692.1
 01700	 71.73	   2261.5
 02500	 72.98	   1810.5
 03500	 68.86	   1608.3
 05000	 69.56	   1391.1
 09000	 66.90	   1221.1
 20000	 69.37	   1039.2

Measuring cache-to-cache transfer latency (in ns)...
Local Socket L2->L2 HIT  latency	25.9
Local Socket L2->L2 HITM latency	29.5
